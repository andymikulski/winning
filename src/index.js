export default {
  scrollingElementPolyfill: () => {
    // scrolling element polyfill
    /*! https://mths.be/scrollingelement v1.5.2 by @diegoperini & @mathias | MIT license */
    if (!('scrollingElement' in document)) {
      function computeStyle(element) {
        if (window.getComputedStyle) {
          // Support Firefox < 4 which throws on a single parameter.
          return getComputedStyle(element, null);
        }
        // Support Internet Explorer < 9.
        return element.currentStyle;
      }

      function isBodyElement(element) {
        // The `instanceof` check gives the correct result for e.g. `body` in a
        // non-HTML namespace.
        if (window.HTMLBodyElement) {
          return element instanceof HTMLBodyElement;
        }
        // Fall back to a `tagName` check for old browsers.
        return /body/i.test(element.tagName);
      }

      function getNextBodyElement(frameset) {
        // We use this function to be correct per spec in case `document.body` is
        // a `frameset` but there exists a later `body`. Since `document.body` is
        // a `frameset`, we know the root is an `html`, and there was no `body`
        // before the `frameset`, so we just need to look at siblings after the
        // `frameset`.
        var current = frameset;
        while (current = current.nextSibling) {
          if (current.nodeType == 1 && isBodyElement(current)) {
            return current;
          }
        }
        // No `body` found.
        return null;
      }

      // Note: standards mode / quirks mode can be toggled at runtime via
      // `document.write`.
      var isCompliantCached;
      var isCompliant = function() {
        var isStandardsMode = /^CSS1/.test(document.compatMode);
        if (!isStandardsMode) {
          // In quirks mode, the result is equivalent to the non-compliant
          // standards mode behavior.
          return false;
        }
        if (isCompliantCached === void 0) {
          // When called for the first time, check whether the browser is
          // standard-compliant, and cache the result.
          var iframe = document.createElement('iframe');
          iframe.style.height = '1px';
          (document.body || document.documentElement || document).appendChild(iframe);
          var doc = iframe.contentWindow.document;
          doc.write('<!DOCTYPE html><div style="height:9999em">x</div>');
          doc.close();
          isCompliantCached = doc.documentElement.scrollHeight > doc.body.scrollHeight;
          iframe.parentNode.removeChild(iframe);
        }
        return isCompliantCached;
      };

      function isRendered(style) {
        return style.display != 'none' && !(style.visibility == 'collapse' &&
          /^table-(.+-group|row|column)$/.test(style.display));
      }

      function isScrollable(body) {
        // A `body` element is scrollable if `body` and `html` both have
        // non-`visible` overflow and are both being rendered.
        var bodyStyle = computeStyle(body);
        var htmlStyle = computeStyle(document.documentElement);
        return bodyStyle.overflow != 'visible' && htmlStyle.overflow != 'visible' &&
          isRendered(bodyStyle) && isRendered(htmlStyle);
      }

      var scrollingElement = function() {
        if (isCompliant()) {
          return document.documentElement;
        }
        var body = document.body;
        // Note: `document.body` could be a `frameset` element, or `null`.
        // `tagName` is uppercase in HTML, but lowercase in XML.
        var isFrameset = body && !/body/i.test(body.tagName);
        body = isFrameset ? getNextBodyElement(body) : body;
        // If `body` is itself scrollable, it is not the `scrollingElement`.
        return body && isScrollable(body) ? null : body;
      };

      if (Object.defineProperty) {
        // Support modern browsers that lack a native implementation.
        Object.defineProperty(document, 'scrollingElement', {
          'get': scrollingElement
        });
      } else if (document.__defineGetter__) {
        // Support Firefox ≤ 3.6.9, Safari ≤ 4.1.3.
        document.__defineGetter__('scrollingElement', scrollingElement);
      } else {
        // IE ≤ 4 lacks `attachEvent`, so it only gets this one assignment. IE ≤ 7
        // gets it too, but the value is updated later (see `propertychange`).
        document.scrollingElement = scrollingElement();
        document.attachEvent && document.attachEvent('onpropertychange', function() {
          // This is for IE ≤ 7 only.
          // A `propertychange` event fires when `<body>` is parsed because
          // `document.activeElement` then changes.
          if (window.event.propertyName == 'activeElement') {
            document.scrollingElement = scrollingElement();
          }
        });
      }
    }
  },
  // bound events = all the events bound on the window
  // {'event name': [handler1, handler2, ...]}
  boundEvents: {},

  // named events = bindings with named aliases for unbinding later
  // event name : ['event type|index of bound event', ...]
  // {'myNamedThing': ['scroll|4', 'resize|1']}
  namedEvents: {},
  flaggedEvents: {},
  // used to pass event data back to throttled function
  eventData: {},

  // allow for disabling window events
  isLooping: false,

  // flag for checking if we have the raf shim in place or not
  _hasInit: false,

  /**
   * Constructor/init function
   * Basically just runs an raf shim to make sure we can use that
   * @return {Winning} this
   */
  checkInit() {
    if (!this._hasInit) {
      this._hasInit = true;
      this.scrollingElementPolyfill();
      this._scrollingEl = document.scrollingElement || document.body;
      this._rafShim();
      this._initLoop();
    }

    return this;
  },


  /**
   * Window binding function
   * Binds an event on the window and throttles via RAF execution.
   * `eventName` will typically be `scroll` or `resize. `eventHandler` is the
   * handling function for the event.
   *
   * `bindingName` is optional and can be used to later `unbind` events.
   * If the same binding name is passed for multiple events, those events
   * are unbound later as a whole (binding is additive, not replace..ive?)
   *
   * @param  {string}   eventName    Name of the event to bind on the window
   * @param  {function} eventHandler Function to fire when the event is triggered
   * @param  {string}   bindingName  Optional: name to use as a reference to group events or unbind later
   * @return {Winning}                Winning
   */
  bindEvent(eventName, eventHandler, bindingName) {
    var winning = this,
      eventName = eventName.split(' '),
      // loop variables
      tempEvtName, i;

    winning.checkInit();

    // event names can be sent in as 'scroll' or 'scroll resize' etc,
    // so we split it up by spaces and loop through em thattaway
    for (i = eventName.length - 1; i >= 0; i--) {
      tempEvtName = eventName[i];

      // check if winning is even listening for this event
      if (!winning.boundEvents.hasOwnProperty(tempEvtName)) {
        // if it's not then we need to add a listener for it
        winning._registerNewEvent(tempEvtName);
      }

      // add the function handler to the list
      winning.boundEvents[tempEvtName].push(eventHandler);

      // if using a name to reference the binding,
      // we need to store the position in which the handler was just added
      // (we use an array so you can bind multiple things together in one group)
      //
      // eg:
      // .bindEvent('resize', handler1, 'myNamedBinding');
      // .bindEvent('scroll', handler2, 'myNamedBinding');
      // .unbind('myNamedBinding'); // both are unbound
      if (bindingName) {
        if (eventName.indexOf(bindingName) > -1) {
          console && console.warn && console.warn('Winning : bind : binding name "' + bindingName + '" can not be the same name as the event ("' + eventName + '")');
          return winning;
        }

        // init the named binding list if needed
        if (!winning.namedEvents.hasOwnProperty(bindingName)) {
          winning.namedEvents[bindingName] = [];
        }

        // add the named binding (in the format of 'event|index') to the group
        winning.namedEvents[bindingName].push(tempEvtName + '|' + winning.boundEvents[tempEvtName].length);
      }
    }

    return winning;
  },

  on(eventName, eventHandler, bindingName) {
    return this.bindEvent(eventName, eventHandler, bindingName);
  },


  /**
   * Unbind event function
   * eventName can be an event ('resize', 'scroll') or binding name ('myBoundEvents') etc
   * @param  {string} eventName Event type or binding reference to unbind
   * @return {Winning}           Winning
   */
  unbindEvent(eventName) {
    var winning = this,
      // loop var
      evt, foundHandlers, currentHandler;

    winning.checkInit();

    if (!eventName || eventName === '') {
      // no event name? unbind all!
      winning.unbindAll();

      // see if the event exists on boundEvents..
    } else if (winning.hasBinding(eventName)) {
      // if so, see if there is an arry of events
      foundHandlers = winning.boundEvents[eventName];

      // set the length to 0 regardless of current members,
      // since we're unbinding ALL of them
      if (foundHandlers) {
        foundHandlers.length = 0;
      }

      // remove the flag for this event
      delete winning.flaggedEvents[eventName];

      // see if the event exists as a named binding..
    } else if (winning.hasNamedBinding(eventName)) {
      winning._unbindNamed(eventName);
    } else {
      console && console.warn && console.warn('Winning : unbind : no "' + eventName + '" to unbind');
    }

    return winning;
  },

  off(eventName) {
    return this.unbindEvent(eventName);
  },

  /**
   * Unbinds everything!
   * @return {Winning} Winning
   */
  unbindAll() {
    var winning = this;

    for (var eventType in winning.boundEvents) {
      var currentEvents = winning.boundEvents[eventType];
      if (currentEvents) {
        currentEvents.length = 0;
      }
    }

    for (var eventType in winning.namedEvents) {
      var currentEvents = winning.namedEvents[eventType];
      if (currentEvents) {
        currentEvents.length = 0;
      }
    }

    for (var eventType in winning.flaggedEvents) {
      var currentEvents = winning.flaggedEvents[eventType];
      if (currentEvents) {
        currentEvents.length = 0;
      }
    }

    return winning;
  },


  /**
   * Detect if the window has listeners for a particular event.
   *
   * @param  {string}   eventName
   * @return {boolean}  Is Winning listening for this?
   */
  hasBinding(eventName) {
    var winning = this;
    return winning.boundEvents.hasOwnProperty(eventName);
  },

  /**
   * Detect if Winning has a named binding/group
   * @param  {string} bindingName Name of binding to look up
   * @return {boolean}            Is Winning listening for this?
   */
  hasNamedBinding(bindingName) {
    var winning = this;
    return winning.namedEvents.hasOwnProperty(bindingName);
  },

  /**
   * Interal function to unbind a named event
   * Looks up a named binding and removes corresponding handler functions
   * from the various handler arrays based on indices etc.
   *
   * @param  {string} eventName Binding name to find and remove
   * @return {Winning}           Winning
   */
  _unbindNamed(eventName) {
    var winning = this,
      foundEvents = winning.namedEvents[eventName],
      currentEvent, currentType, currentIndex;

    if (foundEvents && foundEvents.length) {
      // for each of the events..
      for (var i = foundEvents.length - 1; i >= 0; i--) {
        // take it out of type|index format
        currentEvent = foundEvents[i].split('|');
        currentType = currentEvent[0];
        currentIndex = currentEvent[1] - 1;

        // see if this type even exists..
        if (winning.boundEvents.hasOwnProperty(currentType)) {
          // and if it does, then set our index to null
          // (while this maintains the size of the array, we don't have to
          // worry about juggling around changing indices)
          winning.boundEvents[currentType][currentIndex] = null;
        }
      }

      // remove the property off the namedEvents object all together
      delete winning.namedEvents[eventName];
    }

    return winning;
  },

  /**
   * Binds an event to the window and inits dirty flags
   * @param  {string} eventName [description]
   * @return {Winning}           this
   */
  _registerNewEvent(eventName) {
    var winning = this;

    // init the array of handler functions
    winning.boundEvents[eventName] = [];

    const isTick = eventName === 'tick';

    // bind the new event to the window
    // (so we can set the dirty flag)
    // there is no 'tick' event, so we dont need to bind to the window
    if (!isTick) {
      window.addEventListener(eventName, function(evt) {
        winning._windowEventHandler(eventName, evt);
      });
    }

    // if it's not a tick, it's not flagged by default
    // else it'll be true, and the flag will remain dirty
    // FOREVER
    winning.flaggedEvents[eventName] = isTick;
    winning.eventData[eventName] = {};

    return winning;
  },


  /**
   * Window event handler - the only actual one bound to the window
   * Listens for events to be triggered and then sets the 'flagged' status
   *
   * @param  {string} eventType Event type
   * @param  {object} evt       Native event object
   * @return {Winning}           Winning
   */
  _windowEventHandler(eventType, evt) {
    var winning = this;

    if (winning.flaggedEvents.hasOwnProperty(eventType)) {
      winning.flaggedEvents[eventType] = true;
      winning.eventData[eventType] = evt;
    }

    return winning;
  },

  _getWindowInfo() {
    var winning = this,
      win = window,
      info = {
        winHeight: window.innerHeight,
        winWidth: window.innerWidth,
        scrollTop: winning._scrollingEl.scrollTop,
        scrollRight: winning._scrollingEl.scrollLeft + window.innerWidth,
        scrollBottom: winning._scrollingEl.scrollTop + window.innerHeight,
        scrollLeft: winning._scrollingEl.scrollLeft,
        scrollHeight: winning._scrollingEl.scrollHeight
      };

    return info;
  },

  /**
   * Internal function to loop through dirty events and fire as necessary
   * @return {winning} this
   */
  _fireEvents() {
    var winning = this,
      eventObject = winning.eventData,
      dirtyEvents = winning._getDirty(),
      windowInfo,
      // loop variables
      handlerQueue, handlerFunc, i, j;

    for (i = dirtyEvents.length - 1; i >= 0; i--) {
      // grab the reference to the array
      handlerQueue = winning.boundEvents[dirtyEvents[i]];
      // check if it exists and if it has anything in it..
      if (handlerQueue && handlerQueue.length) {

        // if so then loop through each and run em
        for (j = handlerQueue.length - 1; j >= 0; j--) {
          handlerFunc = handlerQueue[j];
          // bonus function check too
          handlerFunc && typeof handlerFunc === 'function' && handlerFunc(eventObject[dirtyEvents[i]], windowInfo = windowInfo || winning._getWindowInfo());
        }
      }
    }

    return winning;
  },


  /**
   * Internal function to reset the flagged events all to false
   * (resets the status on each loop to listen for fresh triggers)
   * @return {Winning}   Winning
   */
  _resetFlags() {
    var winning = this;

    for (var evt in winning.flaggedEvents) {
      if (winning.flaggedEvents.hasOwnProperty(evt)) {
        // for 'tick' events we dont want to remove the flag
        // we want it to fire on each frame
        if (evt !== 'tick') {
          winning.flaggedEvents[evt] = false;
          winning.eventData[evt] = null;
        }
      }
    }

    return winning;
  },


  /**
   * Get a list of all the currently dirty events
   *
   * @return {Array} List of dirty events
   */
  _getDirty() {
    var winning = this,
      flagObject = winning.flaggedEvents,
      dirty = [];
    for (var evt in flagObject) {
      if (flagObject[evt] === true) {
        dirty.push(evt);
      }
    }
    return dirty;
  },

  //
  //
  //  RAF Init/Tick functions
  //
  //

  /**
   * Internal function to kick off the request animation frame loop
   * @return {Winning} Winning
   */
  _initLoop() {
    var winning = this;

    winning.isLooping = true;
    winning._onTick();

    return winning;
  },

  /**
   * Internal requestAnimationFrame event handler
   * @return {Winning} Winning
   */
  _onTick() {
    var winning = this;

    // check if we stopped looping for some reason
    if (!winning.isLooping) {
      return winning;
    }

    // handle any dirty events,
    // then reset the dirty flags back to normal
    winning._fireEvents()._resetFlags();

    // when we're done we can RAF again
    window.requestAnimationFrame(winning._onTick.bind(winning));
    return winning;
  },

  /**
   * requestAnimationFrame shim
   * @return {void}
   */
  _rafShim() {
    var lastTime = 0,
      vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
      window.cancelAnimationFrame =
        window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
      window.requestAnimationFrame = (callback, element) => {
        var currTime = new Date().getTime(),
          timeToCall = Math.max(0, 16 - (currTime - lastTime)),
          id = window.setTimeout(() => {
            callback(currTime + timeToCall);
          }, timeToCall);
        lastTime = currTime + timeToCall;
        return id;
      };

    if (!window.cancelAnimationFrame)
      window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
      };
  },

  /**
   * Start Winning's monitoring
   * @return {Winning} Winning
   */
  start() {
    return this._initLoop();
  },
  /**
   * Pause Winning's monitoring
   * @return {Winning} Winning
   */
  pause() {
    this.isLooping = false;
  },
  /**
   * Stop Winning's monitoring
   * @return {Winning} Winning
   */
  stop() {
    return this.pause();
  },
  /**
   * Resume Winning's monitoring
   * @return {Winning} Winning
   */
  resume() {
    return this.start();
  }
};