### [1.0.8](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.8)

- Added document.scrollingElement polyfill - was breaking in firefox. whoops!

### [1.0.7](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.7)

- Fixed issue where a binding would fire immediately after being bound, regardless if the event occurred

### [1.0.6](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.6)

- Added scrollHeight info

### [1.0.5](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.5)

- Fixed NPM complaining about document

### [1.0.4](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.4)

- Added general window info object that's passed into each event

### [1.0.3](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.3)

- Actually fixed the init stuff this time

### [1.0.2](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.2)

- Fixed 'init' stuff not running on start

### [1.0.1](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.1)

- Added missing `on`/`off` aliases for `bind`/`unbind` (respectively)


### [1.0.0](https://gitlab.com/andymikulski/winning/releases/tag/v1.0.0)

- Initial release
