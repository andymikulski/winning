# Winning
## rAF-based window event manager

Simple module to bind events to the Window, while throttling via requestAnimationFrame. Instead of defining a millisecond-based throttle, events only fire on animation frames, when the browser is ready. Simple concept, effective results.

## Usage

Include `winning.js` or `winning.min.js` on the page, or install via `npm install winning --save`

Here's a quick example of how to use Winning:

```
Winning.bind('mousemove', function(){ console.log('Winning'); });
window.addEventListener('mousemove', function(){ console.log('Regular'); });

// Winning tracking can be started/paused for finer control
Winning.start();
```

And the results:

![The Results](img/example.png)

As you can see, the native window handler fires off a lot more than our rAF-throttled version.

## Named Bindings

Named bindings offer a convenient way to remove

```
Winning.bind('mousemove', myHandler, 'move-handler');
Winning.bind('mousemove', myHandler, 'other-handler');
```

.. later ..

```
Winning.unbind('move-handler');
// `other-handler` is still goin
```


## Unbinding Events


Unbind a named event

```
Winning.unbind('your-named-event');
```

Unbind all events of a certain type

```
Winning.unbind('mousemove');
```

Unbind all events (named + unnamed)

```
Winning.unbind();
// -or-
Winning.unbindAll();
```


## Tracking

You can control when Winning listens for events! Woah!

```
Winning.start();
Winning.stop();
Winning.pause();
Winning.resume();
```

===


### Credits

Developed by [Andy Mikulski](http://www.andymikulski.com/).

### License
The MIT License (MIT)

Copyright (c) 2016 Andy Mikulski

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.